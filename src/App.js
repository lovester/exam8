import React, { Component } from 'react';
import {BrowserRouter, Route, Switch, NavLink as RouterNavLink} from "react-router-dom";
import AddQuote from "./containers/AddQuote";
import './App.css';
import {Collapse, Container, Nav, Navbar, NavbarBrand, NavbarToggler, NavItem, NavLink} from "reactstrap";
import QuotesList from "./containers/QuotesList";
import EditQuote from "./containers/EditQuote";

class App extends Component {
  render() {
      return (
          <BrowserRouter>
              <Container>
                  <Navbar light expand="md">
                      <NavbarBrand>Quotes Central</NavbarBrand>
                      <NavbarToggler/>
                      <Collapse isOpen navbar>
                          <Nav className="ml-auto" navbar>
                              <NavItem>
                                  <NavLink tag={RouterNavLink} to="/" exact>Quotes</NavLink>
                              </NavItem>
                              <NavItem>
                                  <NavLink tag={RouterNavLink} to="/add">Add quote</NavLink>
                              </NavItem>
                          </Nav>
                      </Collapse>
                  </Navbar>
                  <Switch>
                      <Route path="/" exact component={QuotesList}/>
                      <Route path="/add" exact component={AddQuote}/>
                      <Route path="/quotes/:id/edit" component={EditQuote}/>
                      <Route path="/quotes/:categoryId" component={QuotesList}/>
                      <Route render={() => <h1>Not Found</h1>}/>
                  </Switch>
              </Container>
          </BrowserRouter>
      );
  }
}

export default App;
