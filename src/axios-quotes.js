import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://burger-project-lovester.firebaseio.com/'
});

export default instance;