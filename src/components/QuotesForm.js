import React, {Component} from 'react';

import {CATEGORIES} from "../constants";

import {Button, Col, Form, FormGroup, Input, Label} from "reactstrap";

class QuotesForm extends Component {
    constructor(props) {
        super(props);

        if (props.quotes) {
            this.state = {...props.quotes};
        } else {
            this.state = {
                category: '',
                author: '',
                quoteText: ''
            };
        }
    }

    valueChanged = event => {
        const {name, value} = event.target;

        this.setState({[name]: value});
    };

    submitHandler = event => {
        event.preventDefault();

        this.props.onSubmit({...this.state});
    };

    render() {
        return (
            <Form className="QuotesForm" onSubmit={this.submitHandler}>
                <FormGroup row>
                    <Label for="category" sm={2}>Category</Label>
                    <Col sm={8}>
                        <Input type="select" name="category" id="category"
                               value={this.state.category} onChange={this.valueChanged}>
                            {Object.keys(CATEGORIES).map(categoryId => (
                                <option key={categoryId} value={categoryId}>{CATEGORIES[categoryId]}</option>
                            ))}
                        </Input>
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label for="author" sm={2}>Author</Label>
                    <Col sm={8}>
                        <Input type="text" name="author" placeholder="Author" id="author"
                               value={this.state.author} onChange={this.valueChanged} />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label for="quoteText" sm={2}>Quote text</Label>
                    <Col sm={8}>
                        <Input type="textarea" name="quoteText" placeholder="Quote text" id="quoteText"
                               value={this.state.quoteText} onChange={this.valueChanged} />
                    </Col>
                </FormGroup>
                <FormGroup check row>
                    <Col sm={{ size: 10, offset: 5 }}>
                        <Button color="info" size="lg">Add</Button>
                    </Col>
                </FormGroup>
            </Form>
        );
    }
}

export default QuotesForm;