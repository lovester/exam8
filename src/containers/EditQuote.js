import React, {Component, Fragment} from 'react';
import axios from '../axios-quotes';
import QuotesForm from "../components/QuotesForm";

class EditQuote extends Component {
    state = {
        quotes: null
    };

    componentDidMount() {
        const id = this.props.match.params.id;

        axios.get('/quotes/' + id + '.json').then(response => {
            this.setState({quotes: response.data});
        })
    }

    render() {
        return (
            <Fragment>
                <h1>Edit Quote</h1>
                <QuotesForm onSubmit={this.addQuote} quote={this.state.quotes}/>
            </Fragment>
        );
    }
}

export default EditQuote;