import React, {Component} from 'react';
import {NavLink as RouterNavLink} from "react-router-dom";
import {Button, Card, CardBody, CardText, CardTitle, Col, Nav, NavItem, NavLink, Row} from "reactstrap";

import axios from '../axios-quotes';

import {CATEGORIES} from "../constants";

class QuotesList extends Component {
    state = {
        quotes: null
    };

    loadData() {
        let url = 'quotes.json';

        const categoryId = this.props.match.params.categoryId;

        if (categoryId) {
            url += `?orderBy="category"&equalTo="${categoryId}"`;
        }

        axios.get(url).then(response => {
            const quotes = Object.keys(response.data).map(id => {
                return {...response.data[id], id};
            });

            this.setState({quotes});
        })
    }

    componentDidMount() {
        this.loadData();
    }

    componentDidUpdate(prevProps) {
        if (this.props.match.params.categoryId !== prevProps.match.params.categoryId) {
            this.loadData();
        }
    }

    render() {
        let quotes = null;

        if (this.state.quotes) {
            quotes = this.state.quotes.map(quotes => (
                <Card body outline color="info" style={{marginBottom: "20px"}} key={quotes.id}>
                    <CardBody>
                        <CardTitle>{quotes.author}</CardTitle>
                        <CardText>{quotes.quoteText}</CardText>
                        <RouterNavLink to={'/quotes/' + quotes.id + '/edit'}>
                            <Button color="info" style={{marginRight: "20px"}}>Edit</Button>
                        </RouterNavLink>
                        <Button color="danger">Delete</Button>
                    </CardBody>
                </Card>
                ))
            }

        return (
            <Row>
                <Col sm={3}>
                    <Nav vertical>
                        <NavItem>
                            <NavLink tag={RouterNavLink} to={"/"} exact>All</NavLink>
                        </NavItem>
                    </Nav>
                    <Nav vertical>
                        {Object.keys(CATEGORIES).map(categoryId =>(
                            <NavItem key={categoryId}>
                                <NavLink tag={RouterNavLink} to={"/quotes/" + categoryId} exact>
                                    {CATEGORIES[categoryId]}
                                </NavLink>
                            </NavItem>
                        ))}
                    </Nav>
                </Col>
                <Col sm={9}>
                    {quotes}
                </Col>
            </Row>
        );
    }
}

export default QuotesList;